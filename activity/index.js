// console.log("Hello Worlds!");

let enterNum = prompt("The number you provided is");
console.log("The number you provided is " +enterNum+ ".");

for (let x = enterNum; enterNum >= 0; x--){
    if (x <= 50){
        console.log("The current value is at 50. Terminating the loop.");
        break;
    };
    if (x %10 === 0) {
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    };
    if (x %5 === 0) {
        console.log(x);
        continue;
    };
};

let stringName = "supercalifragilisticexpialidocious";
console.log(stringName);
let result = "";

for (let x = 0; x < stringName.length; x++) {
    if (stringName[x] == "a" || stringName[x] == "e" || stringName[x] == "i" || stringName[x] == "o" || stringName[x] == "u"){
    continue;
    } else {
    result = result += stringName[x];
    }
}   
console.log(result);
